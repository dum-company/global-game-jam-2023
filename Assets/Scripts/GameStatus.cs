using UnityEngine;

[CreateAssetMenu(fileName = "Status", menuName = "Game/Game Status")]
public class GameStatus : ScriptableObject
{
    public bool menu = true;
    public bool game = false;
    public bool paused = false;
}
