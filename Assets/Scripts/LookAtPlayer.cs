using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    Transform player;

    public void Start()
    {
        player = GameObject.Find("First Person Player").transform;
    }

    void Update()
    {
        transform.LookAt(player);
    }
}
