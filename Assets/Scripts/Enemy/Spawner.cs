using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private TeethManager teethManager;
    [SerializeField] GameObject enemyPrefab;
    [SerializeField, Range(0, 50)] private int minEnemiesAmount;
    [SerializeField, Range(0, 20)] private float spawnCooldown;
    private float timer;
    private float count;

    void Start()
    {
        timer = spawnCooldown;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if(timer > spawnCooldown && count < -teethManager.GetTeethAmount() + teethManager.GetMaxTeethAmount() + minEnemiesAmount)
        {
            GameObject enemy = Instantiate(enemyPrefab, transform.position, transform.rotation, transform);
            enemy.GetComponent<EnemyMovement>().Spawner = this;
            timer = 0f;
            count++;
        }
    }

    public void EnemyDied()
    {
        count--;
    }
}
