using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [HideInInspector] public Spawner Spawner;
    [SerializeField] float speed;
    Transform playerTransform;
    PlayerAttacks pA;

    private void Start()
    {
        playerTransform = GameObject.Find("First Person Player").GetComponent<Transform>();
        pA = playerTransform.GetComponent<PlayerAttacks>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pA.puddings.Count == 0)
        {
            transform.LookAt(playerTransform);
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        } else
        {
            transform.LookAt(getNearestPudding());
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
    }

    Transform getNearestPudding()
    {
        Transform nearest = pA.puddings[0].transform;
        foreach (GameObject go in pA.puddings)
            if (Vector3.Distance(nearest.position, transform.position) > Vector3.Distance(go.transform.position, transform.position))
                nearest = go.transform;
        return nearest;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag);
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerHealth>().Damaged(4,true);
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag("Pudding"))
        {
            other.gameObject.GetComponent<ShitPudding>().LifeDamage(2);
        }
    }

    void OnDestroy()
    {
        Spawner.EnemyDied();
    }
}
