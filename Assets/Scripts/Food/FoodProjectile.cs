using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodProjectile : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] private float rootArea = 3;
    [SerializeField] private float explosionArea = 5;
    [SerializeField] GameObject explosionPrefab;

    public void StartMoving(Vector3 direction, float force)
    {
        rb.AddForce(direction * force);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Teeth"))
        {
            Collider[] enemiesCols = Physics.OverlapSphere(transform.position, explosionArea);
            foreach(Collider col in enemiesCols)
            {
                if (col.gameObject.CompareTag("Enemy"))
                {
                    Vector3 explosionPosition = new Vector3(col.gameObject.transform.position.x, col.gameObject.transform.position.y, col.gameObject.transform.position.z);
                    Instantiate(explosionPrefab,explosionPosition, Quaternion.identity);
                    Destroy(col.gameObject);
                }
            }
            Collider[] rootCols = Physics.OverlapSphere(transform.position, rootArea);
            foreach(Collider col in rootCols)
            {
                if (col.gameObject.CompareTag("Teeth") && col.GetComponent<TeethCollider>().IsRoot)
                {
                    col.GetComponent<TeethCollider>().DestroyRoot();
                }
            }
            
            Destroy(gameObject);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, explosionArea);
        Gizmos.DrawWireSphere(transform.position, rootArea);
    }
}
