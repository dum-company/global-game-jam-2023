using UnityEngine;

public class FoodDestroy : MonoBehaviour
{
    [HideInInspector] public FoodSpawner fs;
    void OnDestroy()
    {
        fs.FoodDestroyed();
    }
}
