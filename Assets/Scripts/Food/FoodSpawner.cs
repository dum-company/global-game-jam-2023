using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    [SerializeField] private TeethManager teethManager;
    [SerializeField] GameObject foodPrefab;
    [SerializeField, Range(0, 30)] float spawnCooldown;
    [SerializeField, Range(0, 30)] float foodLifeTime;
    [SerializeField, Range(0, 10)] int maxFoodAmount;
    [SerializeField, Range(0, 5)] int minTeethAmount;
    float timer;
    int counter;


    void Start()
    {
        maxFoodAmount = 5;
        timer = 0f;
    }


    void Update()
    {
        if (teethManager.GetTeethAmount() < minTeethAmount)
            return;

        if (timer < spawnCooldown) {

            timer += Time.deltaTime;
            return;
        }

        if (timer >= spawnCooldown && counter < maxFoodAmount)
            spawnFood();
    }

    private void spawnFood() 
    {
    
        counter++;

        float randomPosX = Random.Range(15, -15); 
        float randomPosZ = Random.Range(15, -15);
        
        float finalPosX = randomPosX + transform.position.x;
        float finalPosZ = randomPosZ + transform.position.z;
        
        GameObject go = Instantiate(foodPrefab, new Vector3(finalPosX, transform.position.y, finalPosZ), Quaternion.identity);
        go.GetComponent<FoodDestroy>().fs = this;
        Destroy(go, foodLifeTime);

        timer -= spawnCooldown;
    }   

    public void FoodDestroyed()
    {
        counter--;
    }

}