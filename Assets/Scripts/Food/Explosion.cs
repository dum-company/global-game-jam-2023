using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] float speed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f); 
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localScale.x < 1f)
        {
            transform.localScale += new Vector3(speed,speed,speed) * Time.deltaTime;
        }
        else
            Destroy(gameObject);
    }
}
