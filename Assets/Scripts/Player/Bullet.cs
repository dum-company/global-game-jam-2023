using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    [SerializeField] Rigidbody body;
    [SerializeField] GameObject shitPudding;
    private PlayerAttacks pA;

    public void StartMoving(Vector3 direction, float force, PlayerAttacks playerAttacks)
    {
        body.AddForce(direction * force);
        pA = playerAttacks;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            GameObject shittyPudding = Instantiate(shitPudding);
            shittyPudding.transform.position = transform.position;
            shittyPudding.GetComponent<ShitPudding>().SetPlayerAttatcks(pA);
            pA.puddings.Add(shittyPudding);
            Destroy(gameObject);

        }
        Destroy(gameObject);
    }
}
