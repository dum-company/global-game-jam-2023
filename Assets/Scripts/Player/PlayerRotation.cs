using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    [SerializeField] InputReader inputReader;
    Vector2 input;

    void Start()
    {
        inputReader.moveEvent += readInput;
    }

    void readInput(Vector2 dir)
    {
        input = dir;
    }

    void Update()
    {
        float rotation = 0;
        if (input.x > 0)
            rotation = (input.y > 0)? 45 : (input.y < 0)? 135 : 90;
        if (input.x < 0)
            rotation = (input.y > 0)? -45 : (input.y < 0)? -135 : -90;
        if (input.x == 0 && input.y > 0)
            rotation = 0;
        if (input.x == 0 && input.y < 0)
            rotation = 180;

        transform.localEulerAngles = Vector3.up * rotation;
    }

    void OnDisable()
    {
        inputReader.moveEvent -= readInput;
    }
}