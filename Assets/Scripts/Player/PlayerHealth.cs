using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private GameStatus gameStatus;
    [SerializeField] private SoundManager sM;
    public float MaxPlayerHealth;
    private float playerHealth;
    public Image HealthBarAmount;
    [SerializeField] private Image playerImage;
    [SerializeField] private Sprite deathPlayer;
    [SerializeField] CinemachineVirtualCamera camera;
    bool dead = false;


    void Start()
    {
        playerHealth = MaxPlayerHealth;
    }

    void Update()
    {    
        HealthBarAmount.fillAmount = playerHealth / MaxPlayerHealth;
        if (playerHealth <= 0){
           Death();
        }
    }

    public void Damaged(int damage, bool shake = false)
    {
        sM.PlaySound("Damaged");
        playerHealth -= damage;
    
        transform.localScale = (new Vector3(playerHealth / MaxPlayerHealth, playerHealth / MaxPlayerHealth, playerHealth / MaxPlayerHealth));
        if(shake) StartCoroutine(Shake());
        if (playerHealth <= 0) 
        {
            Death();
        }
    }
 

    private IEnumerator Shake()
    {
        camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 2f;
        camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 1f;
        yield return new WaitForSeconds(.5f);
        camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0f;
        camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0f;

    }


    public void Heal(int heal) 
    {
        playerHealth += heal;
        if (playerHealth > MaxPlayerHealth) 
        {
            playerHealth = MaxPlayerHealth;
        }
        transform.localScale = (new Vector3(playerHealth / MaxPlayerHealth, playerHealth / MaxPlayerHealth, playerHealth / MaxPlayerHealth));
    }

   

    void Death()
    {
        if (dead)
            return;
        dead = true;
        gameStatus.game = false;
        gameStatus.menu = true;
        SceneManager.LoadSceneAsync("Scenes/DeathScreen");
    }
}
