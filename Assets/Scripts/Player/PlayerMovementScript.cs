using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class PlayerMovementScript : MonoBehaviour
{

    [SerializeField] InputReader inputReader;
    [SerializeField] GameStatus gameStatus;
    [SerializeField] private SoundManager sM;
    [SerializeField] private float stepCooldown;
    private float stepTimer = 0;
    [SerializeField] CharacterController controller;
    [SerializeField] float speed = 12f;
    [SerializeField] float gravity = -9.81f;
    [SerializeField] float jumpHeight = 3f;
    [SerializeField] float jumpForce = 1.5f;
    [SerializeField] Transform groundCheck;
    [SerializeField] Transform characterModelTransform;
    [SerializeField] float groundDistance = 0.4f;
    [SerializeField] LayerMask groundMask;
    [SerializeField] float rotationSpeed;
    [SerializeField] float pushForce;

    public bool EatingTeeth = false;


    Vector3 velocity;
    Vector2 movementInput;
    bool jumping;
    float currentHeight;
    float pushMultiplier;

    private void OnEnable()
    {
        this.inputReader.moveEvent += OnUpdatePosition;
        this.inputReader.jumpEvent += OnJumpInitiated;
        this.inputReader.jumpCanceledEvent += OnJumpCancelled;
        TeethManager.TeethDestroyed += changePushMultipler;
    }

    private void changePushMultipler(float value)
    {
        if (gameStatus.game)
            pushMultiplier = value;
    }

     void Start()
    {
        this.currentHeight = 0f;
        this.EatingTeeth = false;
    }

    void Update()
    {
        this.movementHandler();
        this.gravityHandler();
        this.rotationHandler();
        this.push();
    }


    void movementHandler()
    { 
        Vector3 move = transform.right * this.movementInput.x + transform.forward * this.movementInput.y;
        this.controller.Move(move * this.speed * Time.deltaTime);
        if (movementInput != Vector2.zero && stepTimer > stepCooldown)
        {
            sM.PlaySound("Step");
            stepTimer = 0f;
            return;
        }
        stepTimer += Time.deltaTime;
    }


    void rotationHandler()
    {
         // Change look direction here
    }

    private void push()
    {
        this.controller.Move(Vector3.left * pushForce * pushMultiplier);
    }


    void gravityHandler()
    {
        performGroundedActions();
        if (this.jumping && this.currentHeight < this.jumpHeight) this.performJump();
        else this.cancelJump();
        this.controller.Move(velocity * Time.deltaTime);

    }

    void cancelJump()
    {
        if (!this.checkIfGrounded() && this.currentHeight <= this.jumpHeight)
            this.currentHeight = this.jumpHeight;
        this.velocity.y += this.gravity * Time.deltaTime;
    }

    void performJump()
    {
        this.currentHeight += this.jumpForce * Time.deltaTime;
        this.velocity.y = Mathf.Sqrt(this.currentHeight * -2f * this.gravity);
        
        



    }

    void performGroundedActions()
    {
        if (this.checkIfGrounded() && !this.jumping) this.currentHeight = 0f;
        if (this.checkIfGrounded() && this.velocity.y < 0) this.velocity.y = -2f;
    }

    bool checkIfGrounded()
    {
        return Physics.CheckSphere(this.groundCheck.position, this.groundDistance, this.groundMask);
    }

    void OnJumpInitiated()
    {
        if (gameStatus.game)
        {
            this.jumping = true;
            
        }
    }

    void OnJumpCancelled()
    {
        if (gameStatus.game)
            this.jumping = false;
    }
    
    void OnUpdatePosition(Vector2 movement)
    {
	    if (!gameStatus.game)
	        return;
        if (!EatingTeeth) this.movementInput = movement;
        else movementInput = new Vector2(0f, 0f);
    }

}
