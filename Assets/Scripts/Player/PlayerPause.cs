using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPause : MonoBehaviour
{
    [SerializeField] InputReader inputReader;
    [SerializeField] GameStatus gameStatus;

    void OnEnable()
    {
        this.inputReader.pauseEvent += pause;
    }

    private void pause()
    {
        if (gameStatus.paused)
        {
            gameStatus.game = true;
            gameStatus.paused = false;
            SceneManager.UnloadSceneAsync("Scenes/PauseMenu");
            return;
        }
        gameStatus.game = false;
        gameStatus.paused = true;
        SceneManager.LoadSceneAsync("Scenes/PauseMenu", LoadSceneMode.Additive);
    }

    void OnDisable()
    {
        this.inputReader.pauseEvent -= pause;
    }

}