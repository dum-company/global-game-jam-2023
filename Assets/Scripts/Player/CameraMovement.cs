using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] InputReader inputReader;
    [SerializeField] GameStatus gameStatus;

    [SerializeField] float mouseSensitivity = 100f;
    [SerializeField] Transform playerTransform;
    [SerializeField] Transform cameraFollowTargetTransform;
    [SerializeField] Transform shootingSphereTransform;
    [SerializeField] bool invertView;

    float xRotation = 0f;
    float movementInputX;
    float movementInputY;


    private void OnEnable()
    {
        inputReader.lookEvent += moveCamera;
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void moveCamera(Vector2 movement)
    {
        if (!gameStatus.game)
            return;

        movementInputY = 0f ;
        movementInputY = movement.y;

        movementInputX = movement.x;
    }

    private void Update()
    {
        rotationHandler();
    }

    void rotationHandler()
    {
        float movementX = movementInputX * mouseSensitivity * Time.deltaTime;
        float movementY = movementInputY * mouseSensitivity * Time.deltaTime;

        Vector3 bodyRotation = playerTransform.rotation.eulerAngles;
        bodyRotation.y += movementX;

        if(invertView) xRotation += movementY;
        if(!invertView) xRotation -= movementY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        cameraFollowTargetTransform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        shootingSphereTransform.localRotation = Quaternion.Euler(xRotation, 0, 0);


        playerTransform.rotation = Quaternion.Euler(bodyRotation);


        cameraFollowTargetTransform.Rotate(Vector3.up * movementX);

        
    }
}
