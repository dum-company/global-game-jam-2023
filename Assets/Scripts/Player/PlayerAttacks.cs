using UnityEngine;
using Cinemachine;
using System.Collections.Generic;

public class PlayerAttacks : MonoBehaviour
{
    [SerializeField] InputReader inputReader;
    [SerializeField] GameStatus gameStatus;
    [SerializeField] private SoundManager sM;
    [SerializeField] Transform shootPoint;
    [SerializeField] Transform shootSphere;
    [SerializeField] GameObject shootPrefab;
    public List<GameObject> puddings = new List<GameObject>();
    [SerializeField] float bulletForce;
    [SerializeField] CinemachineVirtualCamera camera;
    [SerializeField] float aimDistance;
    [SerializeField] float disableAimDistance;
    [SerializeField] PlayerHealth localHealth;
    [SerializeField, Range (0, 20)] int damage;
    [SerializeField] PlayerMovementScript movementScript;

    [SerializeField] private GameObject foodProjectile;
    [SerializeField] private Transform foodShootPoint;
    [SerializeField] private GameObject foodImage;
    [SerializeField] private float foodForce;

    private TeethCollider currentTeeth;
    private bool interacting;
    GameObject food;
    bool hasPickedUpFood;


    void Start() 
    {
        localHealth = gameObject.GetComponent<PlayerHealth>();
        currentTeeth = null;
    }

    private void Update()
    {
        if(currentTeeth) Debug.Log("Press E or X to kill the fucker");
        else movementScript.EatingTeeth = false;
    }

    void OnEnable()
    {
        this.inputReader.fireEvent += OnFire;
        this.inputReader.aimEvent += OnAim;
        this.inputReader.aimCanceledEvent += OnAimCancelled;
        this.inputReader.actionEvent += OnAction;
        this.inputReader.actionCancelledEvent += OnActionCancelled;

    }

    void OnFire()
    {
        if (!gameStatus.game || interacting)
            return;
        if (hasPickedUpFood)
        {
            throwFood();
            return;
        }
        sM.PlaySound("ThrowBalls");
        GameObject bullet = Instantiate(shootPrefab, shootPoint.position, Quaternion.identity);
        bullet.GetComponent<Bullet>().StartMoving(shootSphere.forward, bulletForce, this);

        localHealth.Damaged(damage);
    }

    private void throwFood()
    {
        GameObject bullet = Instantiate(foodProjectile, foodShootPoint.position, Quaternion.identity);
        bullet.GetComponent<FoodProjectile>().StartMoving(shootSphere.forward, foodForce);
        hasPickedUpFood = false;
        foodImage.SetActive(false);
    }

    void OnAim()
    {
        
    }

    void OnAimCancelled()
    {
        
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Teeth") currentTeeth = other.GetComponent<TeethCollider>();
        if (other.gameObject.tag == "Food") food = other.gameObject;

    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Teeth") currentTeeth = null;
        if (other.gameObject.tag == "Food") food = other.gameObject;
    }

    void OnAction()
    {
        if (currentTeeth) {
            currentTeeth.isPlayerTouching = true;
            currentTeeth.playerHealth = this.GetComponent<PlayerHealth>();
            currentTeeth.playerAttacks = this;
            movementScript.EatingTeeth = true;
            interacting = true;
            sM.PlaySound("Stick");
        }
        if (food)
        {
            hasPickedUpFood = true;
            foodImage.SetActive(true);
            Destroy(food);
        }
    }

    public void OnActionCancelled()
    {
        if (currentTeeth) {
            currentTeeth.isPlayerTouching = false;
            currentTeeth.playerHealth = null;
            currentTeeth.playerAttacks = null;
            movementScript.EatingTeeth = false;
            interacting = false;
        }
    }

}
