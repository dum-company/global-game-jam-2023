using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] private GameStatus gameStatus;
    [SerializeField] private Slider slider;
    [SerializeField] private AudioMixer mixer;
    float volume;

    void Awake()
    {
        loadConfig();
    }

    private void loadConfig()
    {
        volume = PlayerPrefs.GetFloat("Volume", 1);
        PlayerPrefs.Save();
        
    }

    void Start()
    {
        Time.timeScale = 1;
        slider.value = volume;

        gameStatus.game = false;
        gameStatus.paused = false;
        gameStatus.menu = true;
    }

    public void UpdateVolume(float value)
    {
        volume = value;
        PlayerPrefs.SetFloat("Volume", volume);
        PlayerPrefs.Save();
        mixer.SetFloat("GeneralVolume", Mathf.Log10(value) * 20);
    }

    public void Quit()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #endif
            Application.Quit();
    }

    public void StartGame()
    {
        gameStatus.game = true;
        gameStatus.menu = false;
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
