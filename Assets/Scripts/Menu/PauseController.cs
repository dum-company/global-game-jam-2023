using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseController : MonoBehaviour
{
    [SerializeField] GameStatus gameStatus;

    void Start()
    {
        gameStatus.game = false;
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Continue()
    {
        gameStatus.game = true;
        gameStatus.paused = false;
        SceneManager.UnloadSceneAsync("Scenes/PauseMenu");
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Restart()
    {
        gameStatus.game = true;
        gameStatus.paused = false;
        Cursor.lockState = CursorLockMode.Locked;
        SceneManager.LoadSceneAsync("Scenes/Level1");
    }
    
    public void Menu()
    {
        gameStatus.menu = true;
        gameStatus.paused = false;
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
    }

    void OnDisable()
    {
        Time.timeScale = 1f;
    }
}
