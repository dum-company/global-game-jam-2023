using UnityEngine;

public class TongueMovement : MonoBehaviour
{
    [SerializeField] Transform tongue;
    [SerializeField] float checkArea;
    [SerializeField] float stayingTime;
    float timer;
    [SerializeField] float speed;
    Transform[] positions;
    Transform nextOne;
    Vector3 direction;
    void Start()
    {
        positions = GetComponentsInChildren<Transform>();
    }

    void Update()
    {
        if (isInRange())
        {
            timer += Time.deltaTime;
            if (timer >= stayingTime)
            {
                direction = getDirection();
                timer = 0;
            }
            return;
        }
        move();
    }

    void move()
    {
        tongue.Translate(direction * speed * Time.deltaTime);
    }

    Vector3 getDirection()
    {
        nextOne = getNewPosition();
        return (nextOne.position - tongue.position).normalized;
    }
    
    Transform getNewPosition()
    {
        return positions[Random.Range(0, positions.Length)];
    }

    bool isInRange()
    {
        if (nextOne == null)
            direction = getDirection();
        return (nextOne != null) && (tongue.position.x < nextOne.position.x + checkArea) && (tongue.position.x > nextOne.position.x - checkArea)
         && (tongue.position.y < nextOne.position.y + checkArea) && (tongue.position.y > nextOne.position.y - checkArea)
         && (tongue.position.z < nextOne.position.z + checkArea) && (tongue.position.z > nextOne.position.z - checkArea);
    }
}
