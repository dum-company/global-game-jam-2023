using UnityEngine;
using UnityEngine.UI;

public class TeethHealthDisplay : MonoBehaviour
{
    [SerializeField] private Transform pivot;
    private Image display;
    private float hp, maxHp;

    void Start()
    {
        display = this.GetComponent<Image>();
    }

    public void SetMaxHealth(float value)
    {
        maxHp = value;
    }

    public void SetHealth(float value)
    {
        hp = value;
        display.fillAmount = hp / maxHp;

        display.color = Color.Lerp(Color.red, Color.green, hp / maxHp);
    }

    void Update()
    {
        pivot.LookAt(Camera.main.transform);
    }
}
