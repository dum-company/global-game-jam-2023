using UnityEngine;

public class Root : MonoBehaviour
{
    [SerializeField] private TeethType teeth;

    private MeshCollider meshCollider;
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;


    void Start()
    {
        meshCollider = this.GetComponent<MeshCollider>();
        meshFilter = this.GetComponent<MeshFilter>();
        meshRenderer = this.GetComponent<MeshRenderer>();

        meshCollider.sharedMesh = teeth.rootMesh;
        meshFilter.mesh = teeth.rootMesh;
        meshRenderer.material = teeth.rootMat;
    }

    void OnDrawGizmos()
    {
        meshCollider = this.GetComponent<MeshCollider>();
        meshFilter = this.GetComponent<MeshFilter>();
        meshRenderer = this.GetComponent<MeshRenderer>();

        meshCollider.sharedMesh = teeth.rootMesh;
        meshFilter.mesh = teeth.rootMesh;
        meshRenderer.material = teeth.rootMat;
    }
}
