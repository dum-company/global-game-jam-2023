using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeethCollider : MonoBehaviour
{
    private float hp;
    public bool isPlayerTouching = false;
    public bool IsRoot = false;
    
    [SerializeField] private TeethType teethType;
    [SerializeField] private SoundManager sM;
    [SerializeField] private Transform teeth;
    [SerializeField] private Transform root;
    [Header("Teeth")]
    [SerializeField] private TeethHealthDisplay teethHealthDisplay;
    [SerializeField] private GameObject rootObject;
    [Header("Root")]
    [SerializeField, Range(0, 20)] private int healingAmount;
    [SerializeField, Range(0, 5f)] private float healingCooldown;
    private float timer;
    [HideInInspector] public PlayerHealth playerHealth;
    [HideInInspector] public PlayerAttacks playerAttacks;
    private TeethManager tM;

    void Start()
    {
        hp = teethType.maxHp;
        teethHealthDisplay.SetMaxHealth(teethType.maxHp);
        tM = transform.parent.parent.parent.GetComponent<TeethManager>();
    }

    void Update()
    {
        if (!IsRoot)
            manageTeeth();
        if (IsRoot)
            manageRoot();
        
    }

    private void manageTeeth()
    {
        if (isPlayerTouching)
        {
            hp -= Time.deltaTime;
            teethHealthDisplay.SetHealth(hp);
            if (hp <= 0)
                destroyTeeth();
        }
        else if (hp <= teethType.maxHp)
        {
            hp += Time.deltaTime * teethType.healingMultiplier;
            teethHealthDisplay.SetHealth(hp);
            if (hp > teethType.maxHp)
                hp = teethType.maxHp;
        }
    }
    
    private void destroyTeeth()
    {
        // Explosion
        playerAttacks.OnActionCancelled();
        IsRoot = true;
        rootObject.SetActive(true);
        Destroy(teeth.gameObject);
        sM.PlaySound("TeethExplosion");
        tM.DestroyTeeth();
    }

    private void manageRoot()
    {
        if (!isPlayerTouching)
            return;

        timer += Time.deltaTime;
        if (timer >= healingCooldown)
        {
            playerHealth.Heal(healingAmount);
            timer = 0;
        }
    }

    public void DestroyRoot()
    {
        sM.PlaySound("TeethExplosion");
        root.gameObject.SetActive(false);
        Destroy(transform.parent.gameObject, 7);
        tM.DestroyTeeth();
    }
}
