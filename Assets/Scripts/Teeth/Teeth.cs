using UnityEngine;
using System;

public class Teeth : MonoBehaviour
{
    [SerializeField] private TeethType teeth;

    private MeshCollider meshCollider;
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    void Start()
    {
        meshCollider = this.GetComponent<MeshCollider>();
        meshFilter = this.GetComponent<MeshFilter>();
        meshRenderer = this.GetComponent<MeshRenderer>();

        meshCollider.sharedMesh = teeth.teethMesh;
        meshFilter.mesh = teeth.teethMesh;
        meshRenderer.material = teeth.teethMat;
    }


    void OnDrawGizmos()
    {
        meshCollider = this.GetComponent<MeshCollider>();
        meshFilter = this.GetComponent<MeshFilter>();
        meshRenderer = this.GetComponent<MeshRenderer>();

        meshCollider.sharedMesh = teeth.teethMesh;
        meshFilter.mesh = teeth.teethMesh;
        meshRenderer.material = teeth.teethMat;
    }
}
