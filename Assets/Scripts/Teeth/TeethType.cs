using UnityEngine;

[CreateAssetMenu(fileName = "New Teeth", menuName = "Game/Teeth")]
public class TeethType : ScriptableObject
{
    public float maxHp;
    [Range(0, 1)] public float healingMultiplier;
    public Mesh teethMesh;
    public Material teethMat;
    public Mesh rootMesh;
    public Material rootMat;
}