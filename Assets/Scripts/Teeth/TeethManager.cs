using UnityEngine;
using UnityEngine.Audio;
using System;

public class TeethManager : MonoBehaviour
{
    public static event Action<float> TeethDestroyed;
    [SerializeField] private SoundManager sM;
    [SerializeField, Range(0f, 60f)] float minTime;
    [SerializeField, Range(0f, 60f)] float maxTime;
    [SerializeField, Range(0f, 60f)] float duration;
    float screamingTimer = 0;
    float timer = 0;
    float nextShout;
    bool screaming = false;
    [SerializeField, Range(0, 50)]private int maxAmount;
    [SerializeField] private AudioMixer mixer;
    int counter;

    public int GetTeethAmount()
    {
        return counter;
    }

    public int GetMaxTeethAmount()
    {
        return maxAmount;
    }

    void Start()
    {
        mixer.SetFloat("GritoVolume", -80);
        counter = maxAmount;
        nextShout = UnityEngine.Random.Range(minTime, maxTime);
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= nextShout)
        {
            
            timer = 0;
            nextShout = UnityEngine.Random.Range(minTime, maxTime);
            shout();
        }
        
        if (!screaming)
            return;

        screamingTimer += Time.deltaTime;
        if(screamingTimer >= duration)
            stopShout();
    }

    void shout()
    {
        sM.PlaySound("Grito");
        TeethDestroyed?.Invoke((-(float)counter/(float)maxAmount) + 1);
        screaming = true;
    }

    void stopShout()
    {
        sM.StopSound("Grito");
        TeethDestroyed?.Invoke(0);
        screamingTimer = 0;
        screaming = false;
    }

    public void DestroyTeeth()
    {
        counter--;
        mixer.SetFloat("GritoVolume", Mathf.Log10((-(float)counter/(float)maxAmount) + 1) * 20);
    }
}
