using UnityEngine;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private GameStatus gameStatus;

    void Start()
    {
        Time.timeScale = 1;
    }

    public void StartGame()
    {
        gameStatus.game = true;
        gameStatus.menu = false;
        SceneManager.LoadSceneAsync("Scenes/Level1");
    }
}
